import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import javax.swing.*;

public class appWin extends JFrame {
    private JMenuBar                menuBar;
    private JMenuItem               up,down, key;
    private appPanel                childPanel;
    private JFileChooser            fileChooser;
    private File                    file;

    static public GraphKeyFrame     key_frame;//key-legenda
    static public DataInputStream   fStream;

    appWin() throws IOException {
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize(new Dimension(500,650));

        menuBar = new JMenuBar();

        up = new JMenuItem("Up");
        menuBar.add(up);
        down= new JMenuItem("Down");
        key = new JMenuItem("Show key");
        menuBar.add(key);
        key_frame = new GraphKeyFrame("Graph Key");
        key.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                key_frame.setVisible(true);
            }
        });
        this.setJMenuBar(menuBar);


        fileChooser = new JFileChooser();
        this.showFileChooser();


        childPanel = new appPanel(this);
        this.add(childPanel);

        up.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                childPanel.repaint();
                childPanel.getUpGraph(childPanel.getGraphics());
            }
        });

        down.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                childPanel.repaint();
                childPanel.getDownGraph(childPanel.getGraphics());
            }
        });
        menuBar.add(down);


        this.setVisible(true);

    }

    private void showFileChooser() throws FileNotFoundException {
        fileChooser.setCurrentDirectory(new File("/home/maciej/IdeaProjects/progWin_project1_data"));
        int ret = fileChooser.showOpenDialog(this);

        if(ret == JFileChooser.APPROVE_OPTION){
            System.out.println("You chose to open this file: " +
                    fileChooser.getSelectedFile().getName());
            file = fileChooser.getSelectedFile();
        }

        this.fStream = new DataInputStream(new FileInputStream(file));

    }
}