import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

public class appPanel extends JPanel implements KeyListener {
    private Float[] X;
    private appWin parent;
    private ArrayList<Data> graphsData;
    private Integer index=0;

    appPanel(appWin parent) throws IOException {
        this.X = new Float[120];
        this.graphsData = new ArrayList<Data>();

        this.parent = parent;
        this.setLayout(new GridLayout());
        this.setSize(parent.getSize());
        this.addKeyListener(this);
        this.setFocusable(true);


        this.loadData();
        this.setVisible(true);
    }
    //@UpButtonActionListner
    public void getUpGraph(Graphics g){
        appWin.key_frame.panel.isSet = false;
        appWin.key_frame.panel.texts.clear();
        appWin.key_frame.panel.colors.clear();
        index-=38;
    }
    //@DownButtonActionListner
    public void getDownGraph(Graphics g) {
        index+=38;
        appWin.key_frame.panel.isSet = false;
        appWin.key_frame.panel.texts.clear();
        appWin.key_frame.panel.colors.clear();
        //System.out.print("not implemented yet\n");
    }
    private void drawTable(Graphics g){

        int windowHeight = this.getHeight();
        int windowWidth = this.getWidth();
        int X=windowWidth/4;
        int Y=windowHeight/6;

        //pionowe linie
        for ( Integer i = 0, power = -1; i<windowWidth ;i+=X,power++){
            g.drawLine(X/2 +i,Y/2, X/2 + i,windowHeight-Y/2);
            g.drawString("10",X/2 +i -9,windowHeight-Y/2+20);
            g.drawString(power.toString(),X/2 +i +5,windowHeight-Y/2+10);
        }
        //poziome linie
        for ( Integer i = 0,power = 4;i< windowHeight; i+=windowHeight/6, power--){
            g.drawLine( X/2-5,Y/2+i,windowWidth - X/2,Y/2+i);
            g.drawString("10",X/2-35,Y/2+i+10);
            g.drawString(power.toString(),X/2-20,Y/2+i);
        }

        // linie pomocnicze poziome:
//        for (int i =0;i < 8; i++){
//            g.setColor(new Color(170,180,190));
//            //g.drawLine()
//        }
        // linie pomocnicze logarytnicznie - poziom
        for( int q = 3*Y/2; q < 6*Y ; q+=Y){
            for (double k = 2; k < 9 ; k++){
                int pixel = (int)(Math.log10(k)*Y);
                g.setColor(new Color(118,118,188));
                g.drawLine(  X/2  ,q - pixel-3  , windowWidth - X/2  ,q- pixel-3  );
            }
        }

        // linie pomocnicze logarytmicznie - pion
        for( int q = X/2; q < 3*X ; q+=X){
            for (double k = 2; k < 9 ; k++){
                int pixel = (int)(Math.log10(k)*X);
                g.setColor(new Color(118,118,188));
                g.drawLine( q + pixel+7,  Y/2  , q+pixel+7  ,windowHeight - Y/2   );
            }
        }

    }
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        this.setBackground(new Color(0,255,255));
        this.drawTable(g);
        this.drawGraph(g);
    }

    private void drawGraph(Graphics g) {
        if(index < 38*38)
            index=38*38;
        if(index > 38*38*38)
            index=0;

        System.out.println("index: "+index/38);

        for (int i = 0;i<38;i++,index++){
            Data d = graphsData.get(index);

            g.drawString("D/d"+d.getD_d(),30,20);
            g.drawString("Ri/Rm"+d.getRi_Rm(),150,20);

            g.setColor(Data.colors[i%4]);
            for (int j = 0;j<120-1;j++){
                g.drawLine(calculateXpixel(X[j]),
                       calculateYpixel(d.Y[j]),
                       calculateXpixel(X[j+1]),
                       calculateYpixel(d.Y[j+1])
               );
            }
            if ( appWin.key_frame != null && appWin.key_frame.panel.isSet == false ) {
                appWin.key_frame.panel.addText(d.getRt_Rm().toString(), Data.colors[i % 4]);
            }
        }
        appWin.key_frame.panel.isSet = true;
        index-=38;
    }

    private int calculateXpixel(Float x){
        int X = this.getWidth()/4;
        return (int)(X*x + X/2+X);
    }
    private int calculateYpixel(Float y){
        int Y = this.getHeight()/6;
        return (int)(-Y*y + Y/2 + 4*Y);
    }


    //####################################################################
    private void loadX() throws IOException {
        for ( int i =0 ; i< 120; i++){
            X[i] = Float.intBitsToFloat(Integer.reverseBytes(parent.fStream.readInt()));
        }
    }
    private void loadData() throws IOException {

        this.loadX();

        while( parent.fStream.available() > 0 ){
            Data d = new Data();
            d.loadParams();
            d.loadY();

            graphsData.add(d);
        }
    }


    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if ( keyEvent.getKeyChar() != 'c' )
            return;

        int w = this.getWidth();
        int h = this.getHeight();
        BufferedImage bi = new BufferedImage(
                this.getWidth(),
                this.getHeight(),
                BufferedImage.TYPE_INT_RGB);

        CopyImagetoClipBoard ci = new CopyImagetoClipBoard();
        ci.copyImage(bi);
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }
}