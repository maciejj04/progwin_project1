import java.awt.*;
import java.io.IOException;

/**
 * Created by maciej on 11.03.17.
 */
public class Data {
    public Float[] Y;//zmienic na private
    private Float D_d;
    private Float Ri_Rm;
    private Float Rt_Rm;
    static final Color[] colors = {Color.BLUE,Color.green,Color.red, Color.pink};

    Data(){
        Y = new Float[120];
    }
    public void loadParams() throws IOException {
        D_d = Float.intBitsToFloat(Integer.reverseBytes(appWin.fStream.readInt()));
        Ri_Rm = Float.intBitsToFloat(Integer.reverseBytes(appWin.fStream.readInt()));
        Rt_Rm = Float.intBitsToFloat(Integer.reverseBytes(appWin.fStream.readInt()));
    }
    public void loadY() throws IOException {
        for (int i = 0;i <120; i++){
            Y[i] = Float.intBitsToFloat(Integer.reverseBytes(appWin.fStream.readInt()));
        }
    }

    public Float getD_d() {
        return D_d;
    }

    public Float getRi_Rm() {
        return Ri_Rm;
    }

    public Float getRt_Rm() {
        return Rt_Rm;
    }
}
