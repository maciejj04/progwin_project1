import javax.swing.*;
import java.awt.*;

/**
 * Created by maciej on 11.03.17.
 */
public class GraphKeyFrame extends JFrame {

    public GraphKeyPanel panel;

    @Override
    public void setDefaultCloseOperation(int i) {
        super.setDefaultCloseOperation(i);
        this.setVisible(false);
    }

    GraphKeyFrame(String s){
        this.setDefaultCloseOperation(1);
        this.setSize(new Dimension(200,500));
        panel = new GraphKeyPanel();
        this.add(panel);
     }

}
