import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by maciej on 12.03.17.
 */
public class GraphKeyPanel  extends JPanel {

    //boolean isSet = false;
    ArrayList<String> texts;
    ArrayList<Color> colors;
    boolean isSet = false;

    GraphKeyPanel(){
        texts = new ArrayList<String>();
        colors = new ArrayList<Color>();

        this.setVisible(true);
    }
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        System.out.println("myk");
        drawTexts(g);
    }
    public void addText(String text, Color color){
        texts.add(text);
        colors.add(color);
    }
    private void drawTexts(Graphics g){
        for ( int i = 0; i < texts.size();i++){
            g.setColor(colors.get(i));
            g.drawString(texts.get(i),20,i*15);
        }
    }

}
